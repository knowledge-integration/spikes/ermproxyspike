import groovy.grape.Grape

@GrabResolver(name='mvnRepository', root='http://central.maven.org/maven2/')
@GrabResolver(name='kint', root='http://nexus.k-int.com/content/repositories/releases')
@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.4')
@Grab('commons-codec:commons-codec:1.14')
@Grab('org.ini4j:ini4j:0.5.4')

import groovy.json.JsonSlurper



def platforms = [
  'P1':[
    customizers:[
    ],
    proxies:[
      'P1':{ String input_url -> return 'http://han.sub.uni-goettingen.de/han/vahlen-elibrary/'+input_url.substring(8) }
    ]
  ],
  'P2':[
    customizers:[
      'C2':{ input_url -> "${input_url}&bibd=SUBHH" }
    ],
    proxies:[
    ]
  ]
]

int i=0;

// Load testdata json
def jsonSlurper = new JsonSlurper()
testcases = jsonSlurper.parse(new File('./testcases.json'))
 
testcases.testcases.each { tc ->
  println("\n\nTestcase[${i++}]: ${tc.actual}, platform: ${tc.platform}");
  def platform = platforms[tc.platform]
  if ( platform != null ) {
    result = [
      customized:[:],
      proxied:[:]
    ]

    println("Platform customizers are ${platform.customizers}");
    platform.customizers.each { k, v ->
      result.customized[k] = v.call(tc.actual.toString());
    }

    println("Platform proxies are ${platform.proxies}");
    platform.proxies.each { k, v ->
      // println("Processing proxy ${k} for base URL ${tc.actual} - will be ${v.call(tc.actual.toString()}");
      result.proxied['Actual-'+k] = v.call(tc.actual.toString());

      result.customized.each { ck, cv ->
        println("Processing proxy ${k} for customised url ${cv}");
        result.proxied[ck+'-'+k] = v.call(cv.toString());
      }
    }

    println("Completed test case ${i}: ${tc.actual}");

    result.customized.each { k,v ->
      println("  Customised: [${k}] = ${v}");
    }


    result.proxied.each { k,v ->
      println("  Proxied: [${k}] = ${v}");
    }
  }
  else {
    println("Unable to find platform ${tc.platform}");
    // System.exit(1);
  }
}
