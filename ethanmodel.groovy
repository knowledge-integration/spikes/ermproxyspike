import groovy.grape.Grape

@GrabResolver(name='mvnRepository', root='http://central.maven.org/maven2/')
@GrabResolver(name='kint', root='http://nexus.k-int.com/content/repositories/releases')
@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.4')
@Grab('commons-codec:commons-codec:1.14')
@Grab('org.ini4j:ini4j:0.5.4')

import groovy.json.JsonSlurper



def platforms = [
  'P1':[
  ],
  'P2':[
  ]
]

int i=0;

// Load testdata json
def jsonSlurper = new JsonSlurper()
testcases = jsonSlurper.parse(new File('./testcases.json'))
 
testcases.testcases.each { tc ->
  println("\n\nTestcase[${i++}]: ${tc.actual}, platform: ${tc.platform}");
  def platform = platforms[tc.platform]
  if ( platform != null ) {
    result = [
      customized:[:],
      proxied:[:]
    ]

    // whatever it is
  }
  else {
    println("Unable to find platform ${tc.platform}");
    // System.exit(1);
  }
}
